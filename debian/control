Source: cbflib
Section: science
Priority: optional
Maintainer: Debian PaN Maintainers <debian-pan-maintainers@alioth-lists.debian.net>
Uploaders:
 Morten Kjeldgaard <mok0@ubuntu.com>,
 Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>,
 Teemu Ikonen <tpikonen@gmail.com>,
 Picca Frédéric-Emmanuel <picca@debian.org>,
Build-Depends: dpkg-dev (>= 1.22.5),
 debhelper-compat (= 13),
 dh-sequence-python3,
 libhdf5-dev,
 pkgconf,
 python3-all-dev,
 python3-setuptools,
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/science-team/cbflib
Vcs-Git: https://salsa.debian.org/science-team/cbflib.git
Homepage: http://www.bernstein-plus-sons.com/software/CBF/
Rules-Requires-Root: no

Package: libcbf-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 libcbf1t64 (= ${binary:Version}),
 libhdf5-dev,
 ${misc:Depends},
Description: development files for CBFlib
 CBFLIB is a library of ANSI-C functions providing a simple mechanism for
 accessing Crystallographic Binary Files (CBF files) and Image-supporting
 CIF (imgCIF) files. The CBFLIB API is loosely based on the CIFPARSE API for
 mmCIF files. CBFLIB does not perform any semantic integrity checks and
 simply provides functions to create, read, modify and write CBF
 binary data files and imgCIF ASCII data files.
 .
 This package contains libraries and header files for program development.

Package: libcbf1t64
Provides: ${t64:Provides}
Replaces: libcbf1
Breaks: libcbf1 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: shared library supporting CBFlib
 CBFlib is a library of ANSI-C functions providing a simple mechanism
 for accessing Crystallographic Binary Files (CBF files) and
 Image-supporting CIF (imgCIF) files.
 .
 This package contains the shared library.

Package: cbflib-bin
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: utilities to manipulate CBF files
 CBFlib is a library of ANSI-C functions providing a simple mechanism
 for accessing Crystallographic Binary Files (CBF files) and
 Image-supporting CIF (imgCIF) files.
 .
 This package contains various utility programs.

Package: python3-pycbf
Architecture: any
Section: python
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Python modules for CBFLib -- Python3
 CBFlib is a library of ANSI-C functions providing a simple mechanism
 for accessing Crystallographic Binary Files (CBF files) and
 Image-supporting CIF (imgCIF) files.
 .
 This package contains the Python3 module.

Package: cbflib-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
Enhances:
 libcbf-dev,
Multi-Arch: foreign
Description: documentation for CBFlib
 CBFlib is a library of ANSI-C functions providing a simple mechanism
 for accessing Crystallographic Binary Files (CBF files) and
 Image-supporting CIF (imgCIF) files.
 .
 This package contains the documentation to CBFlib.
